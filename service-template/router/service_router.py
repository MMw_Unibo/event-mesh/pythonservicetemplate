from typing import List
from pydantic import BaseModel
import requests
import math
import time
import json

import paho.mqtt.client as paho

from threading import Thread
from fastapi import APIRouter, status
from config.service_config import ServiceConfiguration


router = APIRouter(
    tags = ["Service"]
)

service_configuration = ServiceConfiguration()
client = None

client = paho.Client()
client.connect(service_configuration.BROKER_ADDRESS, service_configuration.BROKER_PORT)
client.loop_start()


@router.get("/{}/ping".format(service_configuration.SERVICE_NAME.replace(" ", "").replace("_", "").lower()))
async def ping():
    return {"message": "Pong", "source": service_configuration.SERVICE_NAME}


# Model example
# {
#     "id": "1",
#     "target": "serviceb-servicec-serviced",
#     "mode": "one-way",
#     "type": "sync-block",
#     "completed": false,
#     "timestamps": [
#         {
#             "step_name": "service A",
#             "timestamp": 1684084626612
#         }
#     ]
# }

class TimestampEntry(BaseModel):
    step_name: str
    timestamp: int

class SynchRequestBody(BaseModel):
    id: str
    target: str
    mode: str
    type: str
    completed: bool
    timestamps: List[TimestampEntry]

class BurstResponse(BaseModel):
    response: List[SynchRequestBody]

#
# Simple service that add the timestamp when the request is received.
# The service supports a system of route expressed by the target field of the body
#
@router.post("/{}/service/execution".format(service_configuration.SERVICE_NAME.replace(" ", "").replace("_", "").lower()), response_model=SynchRequestBody, status_code = status.HTTP_200_OK)
async def execution(sync_request: SynchRequestBody):
    print("[{} /{}/service/execution]: Received execution request".format(service_configuration.SERVICE_NAME, service_configuration.SERVICE_NAME.replace(" ", "").replace("_", "").lower()))
    print("{}".format(sync_request))

    # Slow down service
    # for i in range(0, 100):
    #     res = i ** int(sync_request.id)
    #     if res % 2 == 0:
    #         res2 = math.sqrt(res)

    #     print("{} {}".format(res, res2))

    # Add timestamp
    sync_request_copy = sync_request.copy(deep=True)
    sync_request_copy.timestamps.append({
        "step_name": service_configuration.SERVICE_NAME,
        "timestamp": round(time.time()*1000)
    })

    # Consume current target and find the next one
    target_ok = "-".join(sync_request_copy.target.split("-")[1:])
    next_target = target_ok.split("-")[0]
    remaining_route = "-".join(target_ok.split("-"))

    response = sync_request_copy
    if sync_request_copy.type == "sync-block":
        response = handleSync(sync_request_copy, next_target, remaining_route)

    else:
        response = handleAsync(sync_request_copy, next_target, remaining_route)

    return response



def handleSync(request: SynchRequestBody, next_target: str, remaining_route: str):
    print("Handle Sync...")
    request.target = remaining_route
    final_response = request
    if next_target == "":
        # Route finished, publish result
        print("Publishing on broker results...")
        client.publish('results', json.dumps(request.dict()), qos=service_configuration.BROKER_QOS)

    else:
        # Route not finished, forward to the service proxy
        print("Forwarding to http://{}:{}/{}/service/execution".format(service_configuration.PROXY_ADDRESS, service_configuration.PROXY_PORT, next_target.lower()))
        # response = requests.post("http://{}:{}/{}/service/execution".format(service_configuration.PROXY_ADDRESS, service_configuration.PROXY_PORT, next_target.lower()), json = request.dict())
        response =  sendToProxy(next_target.lower(), request.dict())

        if response.status_code == 200:
            complete_response = response.json()
            final_response = complete_response

    return final_response


def handleAsync(request: SynchRequestBody, next_target: str, remaining_route: str):
    print("Handle async...")
    imsender = next_target == "" and request.timestamps[0].step_name == service_configuration.SERVICE_NAME
    imlastinchain = next_target == "" and request.timestamps[0].step_name != service_configuration.SERVICE_NAME


    request.target = remaining_route
    final_response = request
    if imsender:
        # Route finished, publish result
        print("IMSENDER")
        print("Publishing on broker results...")
        client.publish('results', json.dumps(request.dict()), qos=service_configuration.BROKER_QOS)

    elif imlastinchain:
        # Forward to first service in chain
        print("IMLASTINCHAIN")
        formatted_service_name = request.timestamps[0].step_name.replace(" ", "").replace("_", "").lower()
        print("Forwarding to http://{}:{}/{}/service/execution".format(service_configuration.PROXY_ADDRESS, service_configuration.PROXY_PORT, formatted_service_name))
        # requests.post("http://{}:{}/{}/service/execution".format(service_configuration.PROXY_ADDRESS, service_configuration.PROXY_PORT, formatted_service_name), json = request.dict())
        sendToProxy(formatted_service_name.lower(), request.dict())

    else:
        # Route not finished, forward to the service proxy
        print("Forwarding to http://{}:{}/{}/service/execution".format(service_configuration.PROXY_ADDRESS, service_configuration.PROXY_PORT, next_target.lower()))
        # requests.post("http://{}:{}/{}/service/execution".format(service_configuration.PROXY_ADDRESS, service_configuration.PROXY_PORT, next_target.lower()), json = request.dict())
        sendToProxy(next_target.lower(), request.dict())

    return final_response


def sendToProxy(target_service, body):
    response = requests.post("http://{}:{}/{}/service/execution".format(service_configuration.PROXY_ADDRESS, service_configuration.PROXY_PORT,target_service), json = body)

    return response