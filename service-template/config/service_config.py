from pydantic import BaseSettings

class ServiceConfiguration(BaseSettings):

    SERVICE_NAME: str = "service A"
    SERVICE_ADDRESS: str = "127.0.0.1"
    SERVICE_PORT: str = "8080"

    PROXY_ADDRESS: str = "127.0.0.1"
    PROXY_PORT: str = "8090"

    BROKER_ADDRESS: str = "127.0.0.1"
    BROKER_PORT: int = 1883
    BROKER_QOS: int = 0