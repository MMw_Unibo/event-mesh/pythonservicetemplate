from fastapi import FastAPI
from router import service_router
from config.service_config import ServiceConfiguration

service_configuration = ServiceConfiguration()

print("########## CONFIGURATION ###################")
print("Service name: {}".format(service_configuration.SERVICE_NAME))
print("Service address: {}".format(service_configuration.SERVICE_ADDRESS))
print("Service port: {}".format(service_configuration.SERVICE_PORT))
print("Proxy address: {}".format(service_configuration.PROXY_ADDRESS))
print("Proxy port: {}".format(service_configuration.PROXY_PORT))
print("############################################")


app = FastAPI()
app.include_router(service_router.router)

